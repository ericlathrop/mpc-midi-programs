# MPC MIDI Programs

MIDI programs for the MPC One, MPC Live, and MPC X firmware 2.10 or later. These
programs correctly name all the MIDI CCs that the each device uses.

# How To Contribute

1. Download
   [Blank.xpm](https://gitlab.com/ericlathrop/mpc-midi-programs/-/raw/main/Blank.xpm).
This is optional, but helps me out by keeping unused CCs named generically. Copy
it to an SD card, and insert into your MPC.
2. Create a new project on your MPC
3. Hook up your MIDI device to your MPC, and verify you can play notes or that
   communication is working.
4. Either load Blank.xpm or create a new MIDI program.
5. Get the manual for your device.
6. Edit the CC names (in the Program Edit button), or map the pads to notes that
   make sense (under the Edit Pad Note Map menu).
8. Make sure the pads and Q-Links work as expected. Reset Q-Links to 0 when
   done.
9. Save the MIDI program to your SD card. Name it after your device's
   manufacturer and model name.
10. Send it to me. If you know how to use git, send me a merge request.
    Otherwise, just [email it to me](mailto:eric@ericlathrop.com). I'll only
accept contributions under [the MIT
License](https://gitlab.com/ericlathrop/mpc-midi-programs/-/raw/main/LICENSE.md).

# Blank.xpm

Start with this when making a new program.

All of the MIDI CCs are named "CC XX" where "XX" is the number of the CC. This
makes it easy to spot unused CCs in your new programs. Initial values are all
set to 0.

# Synthesizers

## Alesis SR-16

[Documentation](https://www.alesis.de/sites/default/files/2018-04/sr16_manual.pdf)

Pads 1-12 on the MPC are mapped to the pads on the Alesis SR-16 in NORMAL mode.
Make sure the Notes / Pad Perform screen (Shift + 16 Levels) is set to "off" for
the notes to be mapped correctly.

## Behringer RD-9

[Documentation](https://mediadl.musictribe.com/media/PLM/data/docs/P0DG6/RD-9_%20M_EN.pdf)

Pads 1-12 on the MPC are mapped to the drum voices on the Behringer RD-9.
Make sure the Notes / Pad Perform screen (Shift + 16 Levels) is set to "off" for
the notes to be mapped correctly.

## Dreadbox Typhon

[Documentation](https://www.dreadbox-fx.com/wp-content/uploads/2020/12/Typhon-Users-Manual.pdf)

## Meeblip Geode

[Documentation](https://meeblip.com/blogs/news/heres-how-to-get-started-with-meeblip-geode)

## Korg Volca Bass

[Documentation](https://i.korg.com/uploads/Support/USA_volcabass_MIDI_Chart_E.pdf)

## Korg Volca Drum

[Documentation](https://cdn.korg.com/us/support/download/files/68ae4f439b41bcb2cdc8350874a84cec.pdf?response-content-disposition=inline%3Bfilename%2A%3DUTF-8%27%27volca_drum_single_ch_MIDI_Chart_E1.pdf&response-content-type=application%2Fpdf%3B)

Programs are included for both split-channel mode (the default setting) and
single-channel mode. For the single-channel mode program, pads 1-6 are mapped to
parts 1-6. Make sure the Notes / Pad Perform screen (Shift + 16 Levels) is set
to "off" for the notes to be mapped correctly.

## Korg Volca Keys

[Documentation](http://i.korg.com/uploads/Support/USA_volcakeys_MIDI_Chart_E.pdf)

## Korg Volca FM

[Documentation](https://cdn.korg.com/us/support/download/files/0ee40bc9fbbb72aa51d519df2af5697d.pdf?response-content-disposition=inline%3Bfilename%3Dvolcafm_MIDI_Chart_E1_01.pdf&response-content-type=application%2Fpdf%3B)
[Pajen's 1.08 Firmware Documentation](https://www.reddit.com/r/volcas/comments/dj7f7v/volca_fm_firmware_108_unofficial_velocity_on_note/)

In addition to naming the official CCs, the extra CCs in Pajen's unofficial
firmware are also named.
